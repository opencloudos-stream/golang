# after we have putting golang into repo, we should shift bootstrap closing.
%bcond_with bootstrap

%global baserelease 1

%global golibdir %{_libdir}/golang

%{!?gopath: %global gopath %{_datadir}/gocode}

# GOROOT
%global goroot          /usr/lib/golang

# https://github.com/golang/go/issues/39466
%ifarch aarch64
%bcond_without enable_tests
%else
%bcond_without enable_tests
%endif

%ifarch x86_64 ppc64le aarch64 loongarch64
%global external_linker 1
%else
%global external_linker 0
%endif

%ifarch x86_64 ppc64le aarch64 loongarch64
%global enable_cgo 1
%else
%global enable_cgo 0
%endif

%ifarch x86_64 ppc64le aarch64
%global enable_shared 1
%else
%global enable_shared 0
%endif

# Pre build std lib with -race enabled
%ifarch x86_64
%global race 1
%else
%global race 0
%endif

%ifarch x86_64
%global gohostarch  amd64
%endif
%ifarch aarch64
%global gohostarch  arm64
%endif
%ifarch ppc64le
%global gohostarch  ppc64le
%endif
%ifarch loongarch64
%global gohostarch  loong64
%endif
%ifarch risv64
%global gohostarch  riscv64
%endif

# https://code.google.com/p/go/issues/detail?id=5238
%global debug_package %{nil}

%global _binaries_in_noarch_packages_terminate_build 0

%global __requires_exclude_from ^(%{_datadir}|/usr/lib)/%{name}/(doc|src)/.*$

%global __strip /bin/true

%define _use_internal_dependency_generator 0
%define __find_requires %{nil}
%global __spec_install_post /usr/lib/rpm/check-rpaths   /usr/lib/rpm/check-buildroot  \
  /usr/lib/rpm/brp-compress



Name:           golang
Version:        1.24.0
Release:        %{baserelease}%{?dist}
Summary:        An open source programming language supported by Google
License:        BSD and Public Domain
URL:            https://go.dev
Source0:        https://go.dev/dl/go%{version}.src.tar.gz
Source2:        golang-gdbinit
Source3:        loongarch64-%{version}.tar.gz
Source4:        loongarch64-%{version}.conf
Source5:        apply-patches

Patch3000:      0001-Modify-go.env.patch
Patch3001:      0002-cmd-link-use-gold-on-ARM-ARM64-only-if-gold-is-avail.patch

# The compiler is written in Go. Needs go(1.4+) compiler for self-building.
%if %{with bootstrap}
BuildRequires:  gcc-go >= 5
%else
BuildRequires:  golang > 1.4
%endif
BuildRequires:  git
BuildRequires:  hostname, net-tools
BuildRequires:  pcre-devel, glibc-static, perl-interpreter, procps-ng
Requires:       %{name}-bin = %{version}-%{release}
Requires:       %{name}-src = %{version}-%{release}
Provides:       go = %{version}-%{release}

ExclusiveArch:  %{golang_arches}

%description
Go is expressive, concise, clean, and efficient. Its concurrency mechanisms 
make it easy to write programs that get the most out of multicore and networked
machines, while its novel type system enables flexible and modular program 
construction. 

Go compiles quickly to machine code yet has the convenience of garbage 
collection and the power of run-time reflection. It's a fast, statically typed,
compiled language that feels like a dynamically typed, interpreted language.

%package       docs
Summary:       Golang compiler docs
Requires:      %{name} = %{version}-%{release}
BuildArch:     noarch

%description   docs
Go is expressive, concise, clean, and efficient. Its concurrency mechanisms 
make it easy to write programs that get the most out of multicore and networked
machines, while its novel type system enables flexible and modular program 
construction. 

Go compiles quickly to machine code yet has the convenience of garbage 
collection and the power of run-time reflection. It's a fast, statically typed,
compiled language that feels like a dynamically typed, interpreted language.

This package includes documents of Go.

%package       tests
Summary:       Golang compiler tests for stdlib
Requires:      %{name} = %{version}-%{release}
BuildArch:     noarch

%description   tests
Go is expressive, concise, clean, and efficient. Its concurrency mechanisms 
make it easy to write programs that get the most out of multicore and networked
machines, while its novel type system enables flexible and modular program 
construction. 

Go compiles quickly to machine code yet has the convenience of garbage 
collection and the power of run-time reflection. It's a fast, statically typed,
compiled language that feels like a dynamically typed, interpreted language.

This package includes tests of Go.

%package        src
Summary:        Golang compiler source tree
BuildArch:      noarch
%description    src
Go is expressive, concise, clean, and efficient. Its concurrency mechanisms 
make it easy to write programs that get the most out of multicore and networked
machines, while its novel type system enables flexible and modular program 
construction. 

Go compiles quickly to machine code yet has the convenience of garbage 
collection and the power of run-time reflection. It's a fast, statically typed,
compiled language that feels like a dynamically typed, interpreted language.

This package includes source code of Go compiler.

%package bin
Summary:        Golang core compiler tools
Provides:       %{name}-go = %{version}-%{release}
Requires(post): %{_sbindir}/update-alternatives
Requires(preun):%{_sbindir}/update-alternatives
Requires:       glibc, gcc
Recommends:     git, subversion, mercurial

%description bin
Go is expressive, concise, clean, and efficient. Its concurrency mechanisms 
make it easy to write programs that get the most out of multicore and networked
machines, while its novel type system enables flexible and modular program 
construction. 

Go compiles quickly to machine code yet has the convenience of garbage 
collection and the power of run-time reflection. It's a fast, statically typed,
compiled language that feels like a dynamically typed, interpreted language.

This package includes compiler and other tools of Go.



%prep
%autosetup -p1 -n go
cp %{SOURCE3} .
cp %{SOURCE4} .
cp %{SOURCE5} .
sh ./apply-patches

# remove this test file for it need network
rm src/crypto/tls/handshake_client_test.go
rm src/crypto/tls/handshake_server_test.go

# remove this test file for it need func testClientHello and testClientHelloFailure
# which defined in file src/crypto/tls/handshake_client_test.go
rm src/crypto/tls/fips_test.go

%build
%if %{with bootstrap}
export GOROOT_BOOTSTRAP=/
%else
export GOROOT_BOOTSTRAP=%{goroot}
%endif

export GOROOT_FINAL=%{goroot}
export GOHOSTOS=linux
export GOHOSTARCH=%{gohostarch}

pushd src
export CFLAGS="%{build_cflags}"
export LDFLAGS="%{build_ldflags}"
export CC="gcc"
export CC_FOR_TARGET="gcc"
export GOOS=linux
export GOARCH=%{gohostarch}
%if !%{external_linker}
export GO_LDFLAGS="-linkmode internal"
%endif
%if !%{enable_cgo}
export CGO_ENABLED=0
%endif

# clean old build cache
"$GOROOT_BOOTSTRAP"/bin/go clean -cache -testcache -modcache -fuzzcache

./make.bash --no-clean -v
popd

%if %{enable_shared}
GOROOT=$(pwd) PATH=$(pwd)/bin:$PATH go install -buildmode=shared -v -x std
%endif

%if %{race}
GOROOT=$(pwd) PATH=$(pwd)/bin:$PATH go install -race -v -x std
%endif



%install
rm -rf %{buildroot}
rm -rf pkg/obj/go-build/*

mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{goroot}

# https://code.google.com/p/go/issues/detail?id=5830
cp -apv api bin doc lib pkg src misc test go.env VERSION \
   %{buildroot}%{goroot}

find %{buildroot}%{goroot}/src -exec touch -r %{buildroot}%{goroot}/VERSION "{}" \;
touch %{buildroot}%{goroot}/pkg
find %{buildroot}%{goroot}/pkg -exec touch -r %{buildroot}%{goroot}/pkg "{}" \;
cwd=$(pwd)

%if %{enable_shared}
pushd %{buildroot}%{goroot}

    mkdir -p %{buildroot}/%{_libdir}/
    mkdir -p %{buildroot}/%{golibdir}/
    for file in $(find .  -iname "*.so" ); do
        chmod 755 $file
        mv  $file %{buildroot}/%{golibdir}
        pushd $(dirname $file)
        ln -fs %{golibdir}/$(basename $file) $(basename $file)
        popd
    done
popd
%endif

rm -rfv %{buildroot}%{goroot}/doc/Makefile

mkdir -p %{buildroot}%{goroot}/bin/linux_%{gohostarch}
ln -sf %{goroot}/bin/go %{buildroot}%{goroot}/bin/linux_%{gohostarch}/go
ln -sf %{goroot}/bin/gofmt %{buildroot}%{goroot}/bin/linux_%{gohostarch}/gofmt

mkdir -p %{buildroot}%{gopath}/src/github.com
mkdir -p %{buildroot}%{gopath}/src/bitbucket.org
mkdir -p %{buildroot}%{gopath}/src/code.google.com/p
mkdir -p %{buildroot}%{gopath}/src/golang.org/x

rm -f %{buildroot}%{_bindir}/go
ln -sf /etc/alternatives/go %{buildroot}%{_bindir}/go
rm -f %{buildroot}%{_bindir}/gofmt
ln -sf /etc/alternatives/gofmt %{buildroot}%{_bindir}/gofmt

mkdir -p %{buildroot}%{_sysconfdir}/gdbinit.d
cp -av %{SOURCE2} %{buildroot}%{_sysconfdir}/gdbinit.d/golang.gdb



%if %{with enable_tests}
%check
export GOROOT=$(pwd -P)
export PATH="$GOROOT"/bin:"$PATH"
cd src

export CC="gcc"
export CFLAGS="%{build_cflags}"
export LDFLAGS="%{build_ldflags}"
%if !%{external_linker}
export GO_LDFLAGS="-linkmode internal"
%endif
%if !%{enable_cgo} || !%{external_linker}
export CGO_ENABLED=0
%endif

export GO_TEST_TIMEOUT_SCALE=10
go clean -cache -testcache -modcache -fuzzcache
./run.bash -no-rebuild -k
cd ..
%endif



%post bin
%{_sbindir}/update-alternatives --install %{_bindir}/go \
    go %{goroot}/bin/go 90 \
    --slave %{_bindir}/gofmt gofmt %{goroot}/bin/gofmt

%preun bin
if [ $1 = 0 ]; then
    %{_sbindir}/update-alternatives --remove go %{goroot}/bin/go
fi



%files
%license LICENSE PATENTS
%doc README.md CONTRIBUTING.md SECURITY.md
%doc %{goroot}/VERSION

%files src
%{goroot}/src/
%{goroot}/api/
%{goroot}/misc/

%files docs
%{goroot}/doc/

%files tests
%{goroot}/test/

%files bin
%{_bindir}/go
%{_bindir}/gofmt
%{goroot}/go.env
%{goroot}/bin/go
%{goroot}/bin/gofmt
%{goroot}/bin/linux_%{gohostarch}/go
%{goroot}/bin/linux_%{gohostarch}/gofmt
%{goroot}/pkg/
%{goroot}/lib/fips140/
%{goroot}/lib/wasm/
%{goroot}/lib/time/
%if %{enable_shared}
%{_libdir}/golang/libstd.so
%endif
%{_sysconfdir}/gdbinit.d


%changelog
* Mon Feb 17 2025 zhaoxiaolin <zhaoxiaolin@loongson.cn> - 1.24.0-1
- [Type] other
- [DESC] upgrade to 1.24.0 for golang 1.22 is EOS, and add new feature support, optimize code, add instructions, etc. on loongarch64

* Thu Jan 09 2025 jackeyji <jackeyji@tencent.com> - 1.22.10-2
- [Type] bugfix
- [DESC] remove handshake test for it need network

* Wed Dec 04 2024 jackeyji <jackeyji@tencent.com> - 1.22.10-1
- [Type] bugfix
- [DESC] upgrade to 1.22.10 for golang 1.21 is EOS

* Wed Oct 30 2024 zhaoxiaolin <zhaoxiaolin@loongson.cn> - 1.21.13-3
- [Type] other
- [DESC] add instructions and code optimization on loongarch64

* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.21.13-2
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Sep 20 2024 jackeyji <jackeyji@tencent.com> - 1.21.13-1
- [Type] security
- [DESC] upgrade to 1.21.13 to fix CVE-2024-24790 CVE-2024-34156 CVE-2024-24789 CVE-2024-24791 CVE-2024-34158 CVE-2024-34155 

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.21.7-11
- Rebuilt for loongarch release

* Tue Jul 30 2024 zhaoxiaolin <zhaoxiaolin@loongson.cn> - 1.21.7-10
- [Type] other
- [DESC] remove architecture check when applying loongarch patches

* Tue Jul 02 2024 zhaoxiaolin <zhaoxiaolin@loongson.cn> - 1.21.7-9
- [Type] other
- [DESC] add BuildRequires git for loongarch64

* Fri Jun 28 2024 zhaoxiaolin <zhaoxiaolin@loongson.cn> - 1.21.7-8
- [Type] other
- [DESC] Fix the way loongarch patches loaded in golang.spec

* Mon May 20 2024 Pengda Dou <doupengda@loongson.cn> - 1.21.7-7
- [Type] other
- [DESC] Close bootstrap

* Tue May 14 2024 jackeyji <jackeyji@tencent.com> - 1.21.7-6
- [Type] other
- [DESC] add go.env and VERSION to bin package

* Wed Apr 24 2024 zhaoxiaolin <zhaoxiaolin@loongson.cn> - 1.21.7-5
- [Type] other
- [DESC] bug fixes and new features support on loongarch64

* Fri Apr 19 2024 jackeyji <jackeyji@tencent.com> - 1.21.7-4
- [Type] security
- [DESC] fix CVE-2023-45288

* Fri Apr 19 2024 Huang Yang <huangyang@loongson.cn> - 1.21.7-3
- enable external_linker cgo and shared lib on loongarch64

* Tue Mar 26 2024 jackeyji <jackeyji@tencent.com> - 1.21.7-2
- fix CVE-2023-45289 CVE-2023-45290 CVE-2024-24783 CVE-2024-24784 CVE-2024-24785

* Mon Mar 18 2024 jackeyji <jackeyji@tencent.com> - 1.21.7-1
- update to 1.21.7 for the last version is end of full support

* Wed Jan 10 2024 jackeyji <jackeyji@tencent.com> - 1.20.6-8
- fix CVE-2023-39325

* Fri Dec 29 2023 jackeyji <jackeyji@tencent.com> - 1.20.6-7
- fix CVE-2023-39318 CVE-2023-39319 CVE-2023-45285

* Mon Dec 11 2023 jackeyji <jackeyji@tencent.com> - 1.20.6-6
- fix CVE-2023-39326

* Mon Dec 11 2023 jackeyji <jackeyji@tencent.com> - 1.20.6-5
- fix CVE-2023-39323

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.20.6-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Sep 01 2023 cunshunxia <cunshunxia@tencent.com> - 1.20.6-2
- fix CVE-2023-29409 CVE-2023-39533

* Mon Jul 31 2023 Shuo Wang <abushwang@tencent.com> - 1.20.6-1
- update to 1.20.6

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.19-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.19-2
- Rebuilt for OpenCloudOS Stream 23

* Fri Oct 28 2022 Zhao Zhen <jeremiazhao@tencent.com> - 1.19-1
- Initial building
